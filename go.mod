module github.com/davdwhyte87/LID-server

go 1.13

require (
	github.com/gorilla/mux v1.8.0
	github.com/kr/pretty v0.2.0 // indirect
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/stripe/stripe-go v70.15.0+incompatible
	github.com/syndtr/goleveldb v1.0.0
	golang.org/x/sys v0.0.0-20190626221950-04f50cda93cb // indirect
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
