package bytes

import (
	"bytes"
	"encoding/binary"
	"encoding/gob"
	"math"
)

// CopyBytes returns a copy of the provided byte slice.
func CopyBytes(b []byte) []byte {
	if b == nil {
		return nil
	}

	cb := make([]byte, len(b))
	copy(cb, b)
	return cb
}

// MarshalBytes marshals [value] into []byte
func MarshalBytes(value interface{}) ([]byte, error) {
	buf := new(bytes.Buffer)
	err := gob.NewEncoder(buf).Encode(value)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

// UnmarshalBytes unmarshals [b] into [v]
func UnmarshalBytes(b []byte, v interface{}) error {
	buf := bytes.NewBuffer(b)
	err := gob.NewDecoder(buf).Decode(v)
	return err
}

// Float64bytes returns a []byte representation of [f]
func Float64bytes(f float64) []byte {
	bits := math.Float64bits(f)
	bytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(bytes, bits)
	return bytes
}

// Float64frombytes returns a float64 representation of [bytes]
func Float64frombytes(bytes []byte) float64 {
	bits := binary.LittleEndian.Uint64(bytes)
	float := math.Float64frombits(bits)
	return float
}
