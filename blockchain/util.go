package blockchain

import (
	"fmt"
	"io/ioutil"
	"os"
	"sync"
	"syscall"
)

var fileMutex sync.Mutex

// ReadFile will read a file from `./data`, returning the bytes
// content
func (h *Handler) ReadFile(path string) ([]byte, error) {
	fileMutex.Lock()
	defer fileMutex.Unlock()

	_, err := os.Stat(path)
	if err != nil && os.IsNotExist(err) {
		return nil, err
	}

	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	err = h.lock(file)
	if err != nil {
		return nil, fmt.Errorf("ReadFile lock failed: %v", err)
	}

	b, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	err = h.unlock(file)
	if err != nil {
		return nil, fmt.Errorf("ReadFile unlock failed: %v", err)
	}
	return b, nil
}

// WriteFile will write to chain concurrently
func (h *Handler) WriteFile(path string, b []byte, perm os.FileMode) error {
	fileMutex.Lock()
	defer fileMutex.Unlock()

	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, perm)
	if err != nil {
		return err
	}
	defer f.Close()

	err = h.lock(f)
	if err != nil {
		return fmt.Errorf("WriteFile lock failed: %v", err)
	}

	_, err = f.Write(b)
	if err != nil {
		return err
	}

	err = h.unlock(f)
	if err != nil {
		return fmt.Errorf("WriteFile unlock failed: %v", err)
	}

	return nil
}

func (h *Handler) lock(file *os.File) error {
	err := syscall.Flock(
		int(file.Fd()),
		syscall.LOCK_EX|syscall.LOCK_NB,
	)
	return err
}

func (h *Handler) unlock(file *os.File) error {
	err := syscall.Flock(
		int(file.Fd()),
		syscall.LOCK_UN,
	)
	return err
}
