package blockchain

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"

	"github.com/davdwhyte87/LID-server/models"
)

// This set of code handles broadcasting requests from this server to other servers
// Broadcast handlers take in paramerets and make new requests

// BCreateWallet ... Broadcast a create wallet command throughout the network
func (h *Handler) BCreateWallet(buildID string, data models.CreateWallet) {
	servers := h.GetServers()
	// n := RandomInt(0, len(servers))
	requestBody, err := json.Marshal(map[string]interface{}{
		"wallet_name":    data.WalletName,
		"wallet_address": data.WalletAddress,
		"private_key":    data.PrivateKey,
		"is_broadcasted": true,
	})

	if err != nil {
		log.Println(err.Error())
		return
	}

	for _, server := range servers {
		if server == h.nodeIP {
			continue
		}

		h.log.Info("[create_wallet]: broadcasting to: " + server)
		e := fmt.Sprintf("http://%s/api/v1/wallet/node/create", server)
		resp, err := h.makeRequest(buildID, "POST", e, bytes.NewBuffer(requestBody))
		if err != nil {
			log.Printf("failed to broadcast request: %v", err)
			continue
		}
		defer resp.Body.Close()
	}
}

// BroadCastTransfer ...
func (h *Handler) BroadCastTransfer(buildID string, data models.TransferReq) {
	h.log.Info("Broadcasting transfer")
	servers := h.GetServers()
	// n := RandomInt(0, len(servers))
	requestBody, err := json.Marshal(map[string]string{
		"sender_private_key":         data.SenderPrivateKey,
		"sender_address":             data.SenderAddress,
		"reciever_address":           data.RecieverAddress,
		"reciever_private_key":       data.RecieverPrivateKey,
		"amount":                     data.Amount,
		"sender_block_id":            data.SenderBlockID,
		"reciever_block_id":          data.RecieverBlockID,
		"reciever_last_block_amount": data.RecieverLastBlockAmount,
		"sender_last_block_amount":   data.SenderLastBlockAmount,
		"reward_validator":           data.RewardValidator,
	})

	if err != nil {
		log.Println(err.Error())
		return
	}

	if len(servers) < 1 {
		return
	}

	x := 3
	for x > 0 {
		x = x - 1
		n := h.RandomInt(0, len(servers)-1)
		peer := servers[n]
		if peer == h.nodeIP {
			continue
		}

		go func(peer string) {
			h.log.Info("broadcasting transfer to %s", peer)
			endpoint := fmt.Sprintf("http://%s/api/v1/wallet/node/transfer", peer)
			resp, err := h.makeRequest(buildID, "POST", endpoint, bytes.NewBuffer(requestBody))
			if err != nil {
				h.log.Error(err.Error())
				return
			}

			var d interface{}
			_ = json.NewDecoder(resp.Body).Decode(&d)
			h.log.Info("resp, statusCode=%v status=%v body=%+v", resp.StatusCode, resp.Status, d)
			defer resp.Body.Close()
		}(peer)
	}
}

// GetServers ...
func (h *Handler) GetServers() []string {
	fileName := "server_list.json"
	file, _ := os.Open(fileName)
	defer file.Close()
	decoder := json.NewDecoder(file)

	type Data []string
	var data Data
	decoder.Decode(&data)

	return data
}

// RandomInt ...
func (h *Handler) RandomInt(min int, max int) int {
	if max < 1 {
		return 0
	}
	rand.Seed(time.Now().Unix())
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	p := r.Perm(max - min + 1)
	return p[min]
}

func shuffle(vals []string) []string {
	// vals := []int{10, 12, 14, 16, 18, 20}
	r := rand.New(rand.NewSource(time.Now().Unix()))
	for len(vals) > 0 {
		n := len(vals)
		randIndex := r.Intn(n)
		vals[n-1], vals[randIndex] = vals[randIndex], vals[n-1]
		//   vals = vals[:n-1]
	}
	return vals
}

func (h *Handler) makeRequest(buildID string, method, endpoint string, body io.Reader) (*http.Response, error) {
	client := &http.Client{Timeout: time.Second * 10}
	req, err := http.NewRequest(method, endpoint, body)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("X-LID-BUILD", buildID)
	req.Header.Add("X-LID-Outbound", "broadcast")
	return client.Do(req)
}
