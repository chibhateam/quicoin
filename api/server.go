package api

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/davdwhyte87/LID-server/api/wallet"
	"github.com/davdwhyte87/LID-server/blockchain"
	"github.com/davdwhyte87/LID-server/database"
	"github.com/davdwhyte87/LID-server/node"
	"github.com/davdwhyte87/LID-server/utils"
	"github.com/davdwhyte87/LID-server/utils/limitter"
	"github.com/davdwhyte87/LID-server/utils/logging"
	"github.com/gorilla/mux"
)

var (
	buildIDHeader        = "X-LID-BUILD"
	broadCastHeader      = "X-LID-Outbound"
	defaultGossipSpacing = 3 * time.Minute
	defaultPeerSampling  = 3
	peerListFile         = "server_list.json"
)

// Server represents the HTTP Server
type Server struct {
	log                     logging.Logger
	logFactory              logging.Factory
	config                  *node.Config
	listenAddress           string
	r                       *mux.Router
	srv                     *http.Server
	chain                   *blockchain.Handler
	priceDB, transactionsDB database.Database
}

// Initialize the http server
func (s *Server) Initialize(
	log logging.Logger,
	logFactory logging.Factory,
	config *node.Config,
	priceDB, transactionsDB database.Database,
) error {
	s.config = config
	s.logFactory = logFactory
	//s.listenAddress = fmt.Sprintf("%s:%s", config.HTTPHost, config.HTTPPort)
	s.listenAddress = fmt.Sprintf(":%s", config.HTTPPort)
	s.r = mux.NewRouter()

	httpLog, err := logFactory.AddDir("HTTP Server", "", true)
	if err != nil {
		return err
	}

	chain, err := blockchain.NewHandler(logFactory, utils.ConnAddr(config))
	if err != nil {
		return err
	}
	s.chain = chain
	s.log = httpLog
	s.priceDB = priceDB
	s.transactionsDB = transactionsDB
	return nil
}

// Dispatch registers all routes and starts the HTTP server
func (s *Server) Dispatch() error {
	l, err := net.Listen("tcp", s.listenAddress)
	if err != nil {
		return err
	}

	// apply rate limitting middleware
	rlimitter := limitter.New(s.log, s.config.MaxBucketSize, s.config.RateLimitCleanupInterval)
	go rlimitter.Cleanup()

	handler := rlimitter.Limit(s.r)
	err = s.initRoutes()
	if err != nil {
		return err
	}

	s.srv = &http.Server{Handler: handler}

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)

	go func() {
		<-quit

		if err := s.Shutdown(); err != nil {
			s.log.Error("failed To ShutDown Server", err)
		}

		s.log.Info("Shutting down HTTP Server")
	}()

	s.log.Info("Running server on %s", s.listenAddress)
	return s.srv.Serve(l)
}

func (s *Server) initRoutes() error {
	// register handlers
	walletsLogger, err := s.logFactory.AddDir("Wallet API", "", true)
	if err != nil {
		return err
	}
	wallets := wallet.NewService(walletsLogger, s.config, s.transactionsDB)

	nr := s.r.PathPrefix("/node").Subrouter() // Node routers
	s.r = s.r.PathPrefix("/api/v1").Subrouter()
	wr := s.r.PathPrefix("/wallet").Subrouter()

	s.r.HandleFunc("/blockchain", s.ArchiveBlocks).Methods("GET")
	s.r.HandleFunc("/price", s.GetPrice).Methods("GET")
	s.r.HandleFunc("/price/history", s.GetPriceHistory).Methods("GET")

	wr.HandleFunc("/balance", wallets.GetBalance).Methods("GET")
	wr.HandleFunc("/create", wallets.CreateWallet).Methods("POST")
	wr.HandleFunc("/print", wallets.PrintBlocks).Methods("POST")
	wr.HandleFunc("/info", wallets.WalletInfo).Methods("POST")
	wr.HandleFunc("/transfer", wallets.TransferLID).Methods("POST")
	// broadacasted channels
	nr.Use(s.extractAndValidateBuildID)
	nr.HandleFunc("/gossip-peer", s.AcceptPeerGossip).Methods("POST")
	nr.HandleFunc("/sync", s.AcceptPeerSync).Methods("POST")
	nr.HandleFunc("/sync-r", s.ProcessSync).Methods("POST")

	wb := wr.PathPrefix("/node").Subrouter()
	wb.Use(s.extractAndValidateBuildID)

	wb.HandleFunc("/balance", wallets.GetBalance).Methods("GET")
	wb.HandleFunc("/create", wallets.CreateWallet).Methods("POST")
	wb.HandleFunc("/print", wallets.PrintBlocks).Methods("POST")
	wb.HandleFunc("/transfer", wallets.TransferLID).Methods("POST")

	return nil
}

func (s *Server) extractAndValidateBuildID(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		isBroadcasted := r.Header.Get(broadCastHeader)
		buildID := r.Header.Get(buildIDHeader)

		if isBroadcasted != "" && buildID != "" {
			// compare with current node build
			if buildID != s.config.BuildHash {
				s.log.Error("Invalid node requested with ID %s, killing request", buildID)
				http.Error(w, "Connection not allowed", http.StatusForbidden)
				return
			}
		} else {
			s.log.Error("Invalid node requested with ID %s, killing request", buildID)
			http.Error(w, "Connection not allowed", http.StatusForbidden)
			return
		}

		next.ServeHTTP(w, r)
	})
}

// Shutdown the HTTP Server
func (s *Server) Shutdown() error {
	if s.srv == nil {
		return nil
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	return s.srv.Shutdown(ctx)
}
