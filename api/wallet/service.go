package wallet

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/davdwhyte87/LID-server/blockchain"
	"github.com/davdwhyte87/LID-server/database"
	"github.com/davdwhyte87/LID-server/models"
	"github.com/davdwhyte87/LID-server/node"
	"github.com/davdwhyte87/LID-server/utils"
	safebytes "github.com/davdwhyte87/LID-server/utils/bytes"
	"github.com/davdwhyte87/LID-server/utils/logging"
	"github.com/davdwhyte87/LID-server/utils/snag"
)

// Service represents the wallet controller service
type Service struct {
	log            logging.Logger
	config         *node.Config
	chain          *blockchain.Handler // blockchain event/service handler
	transactionsDB database.Database
}

var defaultLogConfig = logging.Config{
	Directory: logging.DefaultLogDir,
}

// NewService create a new wallet service handler instance
func NewService(log logging.Logger, config *node.Config, transactionsDB database.Database) *Service {
	logFactory := logging.NewFactory(defaultLogConfig)
	chain, err := blockchain.NewHandler(logFactory, utils.ConnAddr(config))
	if err != nil {
		panic(err)
	}

	//err = chain.CreateICOWallet("QC_ICO_WALLET_SECRET")
	//if err != nil {
	//log.Error("failed to create ico wallet: %v", err)
	//}

	return &Service{
		log:            log,
		config:         config,
		chain:          chain,
		transactionsDB: transactionsDB,
	}
}

// CreateWallet ...
func (s *Service) CreateWallet(w http.ResponseWriter, r *http.Request) {
	var reqData models.CreateWallet
	err := utils.DecodeReq(r, &reqData)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot decode on server")
		return
	}

	if reqData.IsBroadcasted && s.chain.WalletExists(reqData.WalletAddress) {
		utils.RespondWithOk(w, "wallet already exists")
		return
	}

	// create wallet
	walletAddress, err := s.chain.CreateWallet(reqData)
	if err != nil {
		s.log.Error("failed to create wallet: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Failed to create wallet: "+err.Error())
		return
	}

	utils.RespondWithJSON(w, http.StatusCreated, map[string]string{
		"status":  "success",
		"address": walletAddress,
	})

	// broadcast new wallet creation
	reqData.WalletAddress = walletAddress
	go s.chain.BCreateWallet(s.config.BuildHash, reqData)
	return
}

// GetBalance retrieves a consensus agreed balance for a wallet
func (s *Service) GetBalance(w http.ResponseWriter, r *http.Request) {
	walletAddress := r.URL.Query().Get("address")
	if walletAddress == "" {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid wallet address")
		return
	}

	// check wallet exist on request node
	if !s.chain.WalletExists(walletAddress) {
		utils.RespondWithError(w, http.StatusBadRequest, "User wallet not found")
		return
	}
	var (
		balance       float64
		totalNodes    int
		acceptedNodes float64 // Nodes that confirm balance
		declinedNodes float64 // Nodes with balance not matching
	)
	block, err := s.chain.LastBlock(walletAddress)
	if err != nil {
		s.log.Error("failed to retrieve balance for %s: %v", walletAddress, err)
		utils.RespondWithError(w, http.StatusBadRequest, "Failed to retreive wallet balance: "+err.Error())
		return
	}

	balance = block.Amount

	// pool wallet balance from other nodes available
	nodes := s.chain.GetServers()
	totalNodes = len(nodes)
	for _, node := range nodes {
		blocks, err := getAddressChain(walletAddress, node)
		if err != nil {
			log.Printf("failed to retrieve wallet chain, err: %v", err)
			continue
		}
		// retrieve last block, and compare amount
		lastblock := blocks[len(blocks)-1]
		if lastblock.Amount == balance {
			acceptedNodes++
		} else {
			declinedNodes++
		}
	}
	if acceptedNodes < 1 {
		utils.RespondWithError(w, http.StatusBadRequest, "Error retrieving wallet balance")
		return
	}
	// retrieve % of agreeing nodes vs disagreeing nodes
	// consensus should be 80% of nodes should agree on value
	consensus := (acceptedNodes / float64(totalNodes)) * 100
	if consensus < 80 {
		utils.RespondWithError(w, http.StatusBadRequest, "Wallet balance not available at this time")
		return
	}

	utils.RespondWithJSON(w, http.StatusOK, map[string]interface{}{
		"status":  "success",
		"balance": balance,
	})
}

func getAddressChain(walletAddress, node string) ([]blockchain.Block, error) {
	data, _ := json.Marshal(map[string]string{
		"Address":    walletAddress,
		"PrivateKey": "",
	})
	resp, err := http.Post(node+"wallet/print", "application/json", bytes.NewBuffer(data))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var blocks []blockchain.Block
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(body, &blocks)
	if err != nil {
		return nil, err
	}
	return blocks, nil
}

// PrintBlocks ...
func (s *Service) PrintBlocks(w http.ResponseWriter, r *http.Request) {
	var reqData models.PrintBlockReq
	err := utils.DecodeReq(r, &reqData)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot decode on server")
		return
	}

	chain, err := s.chain.PrintChain(reqData.Address)
	if err != nil {
		s.log.Error("failed to retreive chain: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot retrieve wallet chain: "+err.Error())
		return
	}

	utils.RespondWithJSON(w, http.StatusOK, chain)
}

// TransferLID ...
func (s *Service) TransferLID(w http.ResponseWriter, r *http.Request) {
	var reqData models.TransferReq
	err := utils.DecodeReq(r, &reqData)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot decode on server")
		return
	}

	if !s.chain.WalletExists(reqData.SenderAddress) {
		s.log.Error("Sender wallet %s does not exist", reqData.SenderAddress)
		snag.SenderWalletNotFound.
			Render(w, reqData.SenderAddress)
		return
	}

	if !s.chain.WalletExists(reqData.RecieverAddress) {
		s.log.Error("Reciever wallet %s does not exist", reqData.RecieverAddress)
		snag.RecipientWalletNotFound.
			Render(w, reqData.RecieverAddress)
		return
	}

	// check if req is coming from user or server
	if reqData.SenderBlockID != "" {
		if s.chain.BlockExists(reqData.SenderAddress, reqData.SenderBlockID) {
			snag.New(
				"Transaction Failed",
				snag.OpBlockNotExist,
				"Cannot process transfer, sender tx block already exist",
				http.StatusBadRequest,
			).Render(w)
			return
		}
	}

	// check if req is coming from user or server
	if reqData.RecieverBlockID != "" {
		// check if block exists
		if s.chain.BlockExists(reqData.RecieverAddress, reqData.RecieverBlockID) {
			snag.New(
				"Transaction Failed",
				snag.OpBlockNotExist,
				"Cannot process transfer, reciever tx block already exist",
				http.StatusBadRequest,
			).Render(w)
			return
		}
	}

	// strat transfer transaction
	s.log.Info("Starting transfer from %s to %s", reqData.SenderAddress, reqData.RecieverAddress)
	amount, _ := strconv.ParseFloat(reqData.Amount, 64)
	var (
		data            models.TrxData
		rewardValidator string
	)
	data.Amount = amount
	if reqData.RecieverBlockID == "" {
		data.IsBroadcasted = false
		rewardValidator = s.config.ValidatorWalletAddress
	} else {
		data.IsBroadcasted = true
		rewardValidator = reqData.RewardValidator
	}

	data.Sender = reqData.SenderAddress
	data.Reciever = reqData.RecieverAddress
	data.RecieverBlockID = reqData.RecieverBlockID
	data.SenderBlockID = reqData.SenderBlockID
	data.SPrivateKey = reqData.SenderPrivateKey
	data.RPrivateKey = reqData.RecieverPrivateKey

	if data.IsBroadcasted {
		data.SenderLastBlockAmount, _ = strconv.ParseFloat(reqData.SenderLastBlockAmount, 64)
		data.RecieverLastBlockAmount, _ = strconv.ParseFloat(reqData.RecieverLastBlockAmount, 64)
	}

	// make transfer
	newData, err := s.chain.Transfer(data)

	if err != nil {
		s.log.Error("failed to process transfer: %v", err)
		snag.ChainTransferFailed.Render(w, err.Error())
		return
	}

	// mint and distribute token
	err = s.issueSubTransfers(data.Sender, rewardValidator)
	if err != nil {
		s.log.Error("failed to issue sub transfers, err: %v", err)
	}

	reqData.RewardValidator = rewardValidator
	reqData.SenderBlockID = newData.SenderBlockID
	reqData.RecieverBlockID = newData.RecieverBlockID
	reqData.RecieverLastBlockAmount = fmt.Sprintf("%f", newData.RecieverLastBlockAmount)
	reqData.SenderLastBlockAmount = fmt.Sprintf("%f", newData.SenderLastBlockAmount)

	err = s.incrTxCount()
	if err != nil {
		s.log.Error("failed to increment transaction count: %v", err)
	}

	s.log.Info("Transfered %vLIDS from %s to %s", amount, reqData.SenderAddress, reqData.RecieverAddress)

	// BreadCast to other servers
	s.chain.BroadCastTransfer(s.config.BuildHash, reqData)

	utils.RespondWithOk(w, "Transfer Complete")
	return
}

func (s *Service) incrTxCount() error {
	tc, err := s.transactionsDB.Get([]byte("transactions_count"))
	if err != nil {
		return err
	}

	c := safebytes.Float64frombytes(tc)
	c += float64(1)
	tc = safebytes.Float64bytes(c)

	err = s.transactionsDB.Put([]byte("transactions_count"), tc)
	if err != nil {
		return err
	}

	return nil
}

// WalletInfo retrieves the wallet info on a node
func (s *Service) WalletInfo(w http.ResponseWriter, r *http.Request) {
	var reqData models.WalletInfoReq
	err := utils.DecodeReq(r, &reqData)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Cannot decode on server")
		return
	}

	walletInfo, err := s.chain.WalletInfo(reqData.Address)
	if err != nil {
		s.log.Error("failed to retrieve wallet info: %v", err)
		e := snag.NotFound
		e.Meta["raw_err"] = err.Error()
		e.Render(w)
		return
	}

	utils.RespondWithJSON(w, http.StatusCreated, map[string]interface{}{
		"status": "success",
		"data":   walletInfo,
	})
}

func (s *Service) issueSubTransfers(transferInitiator string, rewardValidator string) error {
	var rewardAmount float64 = 0.001
	// 10% of reward amount
	senderReward := 0.1 * rewardAmount
	// 40% of reward amount
	validatorReward := 0.4 * rewardAmount
	// 50% of reward amount
	networkReward := 0.5 * rewardAmount

	// process sender reward
	lastSenderBlock, err := s.chain.LastBlock(transferInitiator)
	if err != nil {
		return err
	}
	senderRewardBlock := blockchain.Block{
		ID:             utils.StringWithCharset(12),
		Sender:         s.config.LIDRewardSender,
		Reciever:       transferInitiator,
		Amount:         lastSenderBlock.Amount + senderReward,
		Hash:           createBlockHash(transferInitiator, lastSenderBlock.Hash, lastSenderBlock.Amount+senderReward),
		PrevHash:       lastSenderBlock.Hash,
		PrivateKeyHash: lastSenderBlock.PrivateKeyHash,
	}
	err = s.chain.SaveBlock(transferInitiator, senderRewardBlock)
	if err != nil {
		return err
	}

	// validator reward
	if rewardValidator != "" {
		validatorAddress := rewardValidator
		lastValidatorBlock, err := s.chain.LastBlock(validatorAddress)
		if err != nil {
			return err
		}
		validatorRewardBlock := blockchain.Block{
			ID:             utils.StringWithCharset(12),
			Sender:         s.config.LIDRewardSender,
			Reciever:       validatorAddress,
			Amount:         lastValidatorBlock.Amount + validatorReward,
			Hash:           createBlockHash(validatorAddress, lastValidatorBlock.Hash, lastValidatorBlock.Amount+validatorReward),
			PrevHash:       lastValidatorBlock.Hash,
			PrivateKeyHash: lastValidatorBlock.PrivateKeyHash,
		}
		err = s.chain.SaveBlock(validatorAddress, validatorRewardBlock)
		if err != nil {
			return err
		}
	}

	// network reward
	networkAddress := s.config.LIDBankWalletAddress
	lastNetworkBlock, err := s.chain.LastBlock(networkAddress)
	if err != nil {
		return err
	}
	networkRewardBlock := blockchain.Block{
		ID:             utils.StringWithCharset(12),
		Sender:         s.config.LIDRewardSender,
		Reciever:       networkAddress,
		Amount:         lastNetworkBlock.Amount + networkReward,
		Hash:           createBlockHash(networkAddress, lastNetworkBlock.Hash, lastNetworkBlock.Amount+networkReward),
		PrevHash:       lastNetworkBlock.Hash,
		PrivateKeyHash: lastNetworkBlock.PrivateKeyHash,
	}
	err = s.chain.SaveBlock(networkAddress, networkRewardBlock)
	if err != nil {
		return err
	}

	return nil
}

// TODO: move as util method
func createBlockHash(sender, prevHash string, amount float64) string {
	pts := fmt.Sprintf("%s%s%s", prevHash, fmt.Sprintf("%f", amount), sender)
	sha := sha256.New()
	sha.Write([]byte(pts))
	return hex.EncodeToString(sha.Sum(nil))
}
